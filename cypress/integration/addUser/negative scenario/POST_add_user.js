import '@bahmutov/cy-api'

describe('Try to create new user with incorrect token' , ()=>{
    
    it('POST', ()=> {
        
        cy.request({
            method : 'POST',
            url : 'https://gorest.co.in/public/v2/users',
            body : {
                 name: "Tenali Ramakrishna", 
                 gender: "male", 
                email: "tesdtssdds123@tdaadest.pl" , 
                status: "active"
            },
            headers: {
            'Authorization' : 'Bearer ' + "21334"},
            failOnStatusCode: false,
        })
        .then(function(response) {
            expect(response.status).to.equal(401);
            expect(response.body).to.have.property('message')
        }
    )
       
            


    })



})